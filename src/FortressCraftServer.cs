﻿using System;
using System.Globalization;
using System.Net;
using uMod.Common;
using uMod.Text;
using UnityEngine;

namespace uMod.Game.FortressCraft
{
    /// <summary>
    /// Represents the server hosting the game instance
    /// </summary>
    public class FortressCraftServer : IServer
    {
        /// <summary>
        /// Gets or sets the player manager
        /// </summary>
        public IPlayerManager PlayerManager { get; set; }

        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        public string Name
        {
            get => ServerConsole.WorldName;
            set => ServerConsole.WorldName = value;
        }

        private static IPAddress address;
        private static IPAddress localAddress;

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        public IPAddress Address
        {
            get
            {
                try
                {
                    if (address == null)
                    {
                        uint ip;
                        string serverIp = PersistentSettings.IPAddress;
                        if (Utility.ValidateIPv4(serverIp) && !Utility.IsLocalIP(serverIp))
                        {
                            IPAddress.TryParse(serverIp, out address);
                            Interface.uMod.LogInfo($"IP address from command-line: {address}"); // TODO: Localization
                        }
                        else if ((ip = Steamworks.SteamGameServer.GetPublicIP()) > 0)
                        {
                            string publicIp = string.Concat(ip >> 24 & 255, ".", ip >> 16 & 255, ".", ip >> 8 & 255, ".", ip & 255); // TODO: uint IP address utility method
                            IPAddress.TryParse(publicIp, out address);
                            Interface.uMod.LogInfo($"IP address from Steam query: {address}");
                        }
                        else
                        {
                            WebClient webClient = new WebClient();
                            IPAddress.TryParse(webClient.DownloadString("http://api.ipify.org"), out address);
                            Interface.uMod.LogInfo($"IP address from external API: {address}");
                        }
                    }

                    return address;
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's public IP address", ex);
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the local IP address of the server, if known
        /// </summary>
        public IPAddress LocalAddress
        {
            get
            {
                try
                {
                    return localAddress ?? (localAddress = Utility.GetLocalIP());
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's local IP address", ex);
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        public ushort Port => NetworkServerThread.GAME_PORT;

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        public string Version
        {
            get
            {
                int index = HUDManager.Version.IndexOf(" -", StringComparison.Ordinal);
                return index > 0 ? HUDManager.Version.Substring(0, index) : "Unknown"; // TODO: Localization
            }
        }

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        public string Protocol => Version;

        /// <summary>
        /// Gets the language set by the server
        /// </summary>
        public CultureInfo Language => CultureInfo.InstalledUICulture;

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        public int Players => NetworkManager.instance.mServerThread.GetNumPlayers();

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        public int MaxPlayers
        {
            get => NetworkServerThread.mnPublicMaxPlayerCount;
            set => ServerConsole.MaxPlayers = value;
        }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        public DateTime Time
        {
            get => DateTime.FromOADate(WorldScript.instance.mWorldData.mrCurrentTimeOfDay);
            set => WorldScript.instance.mWorldData.mrCurrentTimeOfDay = value.Second;
        }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        public int FrameRate
        {
            get => Mathf.RoundToInt(1f / UnityEngine.Time.smoothDeltaTime); // TODO: Check for native method
        }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        public int TargetFrameRate
        {
            get => UnityEngine.Application.targetFrameRate; // TODO: Check for native method
            set => UnityEngine.Application.targetFrameRate = value;
        }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo IServer.SaveInfo => throw new NotImplementedException(); // TODO: Implement when possible

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        public void Save() => WorldScript.instance.SaveWorldSettings();

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        public void Shutdown(bool save = true, int delay = 0)
        {
            if (save)
            {
                Save();
            }

            // TODO: Implement optional delay
            GameManager.instance.LeaveWorld(true);
        }

        #endregion Server Administration

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string playerId, string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned(playerId))
            {
                // Ban player
                ulong.TryParse(playerId, out ulong userId);
                NetworkManager.instance.mBanListManager.UnbanPlayer(userId);

                // Kick player if connected
                if (IsConnected(playerId))
                {
                    foreach (NetworkServerConnection connection in NetworkManager.instance.mServerThread.connections)
                    {
                        if (connection.mPlayer != null && connection.mPlayer.mUserID.Equals(playerId))
                        {
                            NetworkManager.instance.mServerThread.KickPlayer(connection, reason);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        public TimeSpan BanTimeRemaining(string playerId) => IsBanned(playerId) ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsBanned(string playerId)
        {
            ulong.TryParse(playerId, out ulong userId);
            return NetworkManager.instance.mBanListManager.CheckBan(userId);
        }

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsConnected(string playerId)
        {
            bool connected = false;
            foreach (NetworkServerConnection connection in NetworkManager.instance.mServerThread.connections)
            {
                if (connection.mPlayer != null && connection.mPlayer.mUserID.Equals(playerId))
                {
                    connected = true;
                }
            }
            return connected;
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        public void Unban(string playerId)
        {
            // Check if already unbanned
            if (IsBanned(playerId))
            {
                // Unban player
                ulong.TryParse(playerId, out ulong userId);
                NetworkManager.instance.mBanListManager.UnbanPlayer(userId);
            }
        }

        #endregion Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, params object[] args)
        {
            message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
            string formatted = prefix != null ? $"{prefix} {message}" : message;
            ChatLine chatLine = new ChatLine
            {
                mPlayer = -1,
                mPlayerName = string.Empty, // TODO: Test if prefix can be left out
                mText = formatted,
                mType = ChatLine.Type.Normal
            };
            NetworkManager.instance.QueueChatMessage(chatLine);
            ServerConsole.DebugLog($"[SERVER] {formatted}");
        }

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        public void Broadcast(string message) => Broadcast(message, null);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            ServerConsole.DoServerString($"{command} {string.Join(" ", Array.ConvertAll(args, x => x.ToString()))}");
        }

        #endregion Chat and Commands
    }
}
